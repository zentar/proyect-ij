import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Gestor
{
    private ArrayList<Abono> ficheroAbonos;
    private double tarifaBase;
    private ArrayList<Estacion> ficheroEstaciones;
    private String estacion;
    
    
    public Gestor(){
      ficheroAbonos= new ArrayList<Abono>();
      tarifaBase=0;
      ficheroEstaciones = new ArrayList<Estacion>();
    }
    
    /*Introducido un DNI devuelve la i, si el DNI si esta registrado i sera la posición del Abono en la lista de abonos
     * si no en i sera -1
    */
    public int buscarAbono (String DNI){
        for (int i=0;i < ficheroAbonos.size(); i++){
            if(DNI==ficheroAbonos.get(i).getDNI()){
                return i;
            }
        }
        return -1;
    }
    public int buscarEstacion (String e){
        for (int i=0; i < ficheroEstaciones.size(); i++){
            if (e == ficheroEstaciones.get(i).getEstacion()){
                return i;
            }
        }
        return -1;
    }
    
    
    
    //Añade a la lista un Abono, si el DNI esta registrado lo notifica y no añade nada
    public void anyadirAbono(Abono a){
        //Recorro los DNI por la lista de abono para ver si el DNI esta ya registrado.
        int pos = buscarAbono(a.getDNI());
        if (pos!=-1){
            System.out.println("El DNI esta ya registrado");
        }else{
            ficheroAbonos.add(a);
            System.out.println("######################################");
            System.out.println("#Se ha añadido correctamente el abono#");
            System.out.println("######################################");
            a.imprimeInformacion();
            System.out.println("######################################\n\n");
        }
    }
    //Añade una estación al fichero de estaciones.
    public void anyadirEstacion(Estacion e){
        int pos = buscarEstacion(e.getEstacion());
        if(pos==-1){
            ficheroEstaciones.add(e);
            System.out.println("Se ha añadido la estación "+ e.getEstacion());
        }else{
            System.out.println("La estación ya esta en el fichero");
        }
    }
    
    
    //Imprime el importe de la recarga introducido un DNI
    public void imprimeImporte(String DNI){
        int pos = buscarAbono(DNI);
        double tarifa;
        if (pos!=-1){
            tarifa=ficheroAbonos.get(pos).calcularImporte(tarifaBase);
            System.out.println("El importe es: "+tarifa);
        }else{
            System.out.println("El usuario de DNI "+DNI+" no tiene abono");
        }
    }
    
    
    //Método de recarga del abono, se actualiza la fecha de caducidad en un mes
    public void recargaAbono (String DNI){
        int pos = buscarAbono(DNI);
        if (pos!=-1){
            ficheroAbonos.get(pos).recarga();
        }else{
            System.out.println("El usuario de "+DNI+" no existe en el sistema");
        }
    }
    
    
    public void imprimirAbono(String DNI){
        //Busqueda del DNI del usuario para la impresion de los datos de su abono
        int pos = buscarAbono(DNI);
        if (pos!=-1){
            ficheroAbonos.get(pos).imprimeInformacion();
        }else{
            System.out.println("El usuario de "+DNI+" no existe en el sistema");
        }
    }
    
    public void zonaRequerida(String z1, String z2){
        int pos1 = buscarEstacion(z1);
        int pos2 = buscarEstacion(z2);
        String zona;
        if (pos1 == -1){
            System.out.println("La estación "+ z1 + " no existe.");
        } else if (pos2 == -1){
            System.out.println("La estación "+ z2 + " no existe.");
        }else{
            if(ficheroEstaciones.get(pos1).getZona().ordinal() < ficheroEstaciones.get(pos2).getZona().ordinal()){
                zona=ficheroEstaciones.get(pos2).getZona().toString();
            }else{
                zona=ficheroEstaciones.get(pos1).getZona().toString();
            }
            System.out.println("El recorrido entre " + z1 + " y "+ z2 +" requiere un abono adscrito a la zona " + ficheroEstaciones.get(pos2).getZona().toString());
        }
    }
    
    public void actualizaTarifaBase(double tarifa){
        tarifaBase = tarifa;
    }
    public void consultaTarifaBase(){
        System.out.println("La tarifa base esta en "+tarifaBase+" euros.");
    }
    
    public void imprimeprecioRecarga(){
        System.out.println("El precio de la recarga es: " +tarifaBase);
    }
    

}
