import java.util.Calendar;
import java.util.Date;
        
        
public class AbonoAnual extends Abono
{
    private int numeroRecarga;
    
    
    public AbonoAnual()
    {
        numeroRecarga = 0;
    }
    
    
    public AbonoAnual(String nombre, String apellidos, String Dni, int anio, boolean familiaNumerosa, int numeroRecarga){
        super(nombre,apellidos,Dni,anio,familiaNumerosa);
    }
  
    @Override
    public void imprimeInformacion(){
        super.imprimeInformacion();
        System.out.println("Numero de recargas "+ numeroRecarga);
        
    }
    
    @Override
    public double calcularImporte(double tarifaBase){
            double tarifa;
        //Calculo de la tarifa segun la zona, el valor se almacena en la variable tarifa
            if (this.getZona()==Zona.A1){
                 tarifa= tarifaBase*10;
            }else if (this.getZona()==Zona.A2){
                 tarifa = tarifaBase*12;
             }else if(this.getZona()==Zona.B1){
                 tarifa = tarifaBase*15;
             }else if(this.getZona()==Zona.B2){
                 tarifa = tarifaBase*20;
             }else{
                 tarifa = tarifaBase*30;
             }
             //Modifico la tarifa si el usuario es familia numerosa
            if(this.getFamiliaNumerosa()==true){
                tarifa = tarifa*0.5;
            }
            
            return tarifa;
        
        }
    @Override
    public void recarga(){
            Calendar d = Calendar.getInstance();//Fecha de hoy en variabe d
            d.add(Calendar.YEAR,1);//le sumo un mes a la fecha
            Date date=d.getTime();//transformo calendar en date
            this.setFechaCaducidad(date);//actualizo la fecha del abono
            numeroRecarga+=1;
            System.out.println("El abono "+ getDNI() +"  ha sido recargado. \nLa nueva fecha de caducidad es: " + date);
            System.out.println("Ha realizado "+numeroRecarga+" recarga(s).");
        }
}
