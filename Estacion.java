import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Estacion{
    String estacion;
    private Zona zonaTarifaria; 
            
    public Estacion(String localidad_distrito){
        estacion = localidad_distrito;
    }
    public Estacion(){
    }
    
    public void setLocalidad(String localidad){
        estacion = localidad;
    }   
    public String getEstacion(){
        return estacion;
    }
    
    public void setZona(Zona z){
        zonaTarifaria = z;
    }
    public Zona getZona(){
        return zonaTarifaria;
    }
    
    public boolean validarAbono (Abono a){
        boolean esValido = false;
    
        Calendar d = Calendar.getInstance();
        Date date=d.getTime();
     
        if(a.getFechaCaducidad().compareTo(date)>0){ 
        
            if ( a.getZona().ordinal() < zonaTarifaria.ordinal()){    //Se comparan las zonas
            System.out.println("La zona del abono "+ a.getDNI() +" no es válida " + "para la estación " +getEstacion());
            esValido=false;
        }
            else {                                                         
            System.out.println("El abono "+a.getDNI()+" es válido para la estación "+getEstacion());
            esValido = true;
        }
    }
        else{
            System.out.println("El abono "+a.getDNI()+" está caducado.");
            esValido=false;
        }
        return esValido;
    }   
    }
    