        import java.util.Calendar;
        import java.util.Date;
        
        /**
         * Clase Abono.
         * 
         * @author Ismael Juarez Garcia
         * @version 1.0
         */
    
          
   public class Abono
        {
            private String nombreTitular;
            private String apellidosTitular;
            private String DNI;
            private int anioNacimiento;
            private boolean familiaNum;
            private Calendar cal;
            private Date fechaCaducidad;
            private Zona zonaTarifaria;
            
        /**
        * Constructor para la clase Abono
        * @param nombre Nombre del titular del abono
        * @param apellidos Apellidos del titular del abono
        * @param Dni Documento Nacional de Identidad del titular
        * @param anio Año de nacimiento
        * @param familiaNumerosa Si el titular pertenece a familia numerosa es true, caso contrario false.
        */      
        public Abono (String nombre, String apellidos, String Dni, int anio, boolean familiaNumerosa)
        {
            nombreTitular = nombre;
            apellidosTitular = apellidos;
            DNI = Dni;
            anioNacimiento = anio;
            familiaNum = familiaNumerosa;                
        /* Al crear el abono inicializo la fecha inicial es anterior a la fecha actual como indica la etapa 2,
        * asi es necesario recargar el abono depues de ser creado para hacer uso de este.
        */
            Calendar cal = Calendar.getInstance();//Fecha de hoy en variabe cal
            cal.add(Calendar.DATE,-1);/*le resto un dia para que inicialice el abono con fecha de caducidad anterior a la acual
                                       * como especifica en la etapa 2
                                       */
            fechaCaducidad=cal.getTime();//transformo calendar en date
        }
        
        
        public Abono ()
        {
            /* Al crear el abono inicializo la fecha inicial es anterior a la fecha actual como indica la etapa 2,
             * asi es necesario recargar el abono depues de ser creado para hacer uso de este.
             */
            Calendar cal = Calendar.getInstance();//Fecha de hoy en variabe cal
            cal.add(Calendar.DATE,-1);/*le resto un dia para que inicialice el abono con fecha de caducidad anterior a la acual
                                       * como especifica en la etapa 2
                                       */
            fechaCaducidad=cal.getTime();//transformo calendar en date
        }

        
        /**
        *Devuelve nombre del titular del Abono.
        *@return Nombre del titular.
        */
       
        public String getNombre(){
            return nombreTitular;
        }
        public void setNombre (String nombre){
            this.nombreTitular=nombre;
        }
        
        
        //Métodos de consulta y modificación de los Apellidos del Titular
        public String getApellidos(){
            return apellidosTitular;
        }
        public void setApellidos(String apellidos){
            this.apellidosTitular=apellidos;
        }
        
        
        //Método de consulta y modificación del DNI del titular
        public String getDNI(){
            return DNI;
        }
        public void setDNI(String DNI){
            this.DNI=DNI;   
        }
        
       
        //Métodos de consulta y modificación del Año de Nacimiento
        public int getAnioNacimiento(){
            return anioNacimiento;
        }
        public void setAnioNacimiento(int año){
            this.anioNacimiento=año;
        }
        
        
        //Metodos de consulta y modificación de Condición de Familia Numerosa
        public boolean getFamiliaNumerosa(){
           return familiaNum;
        }
        public void setFamiliaNumerosa(boolean familiaNum){
            this.familiaNum=familiaNum;
        }
        
        
        //Metodos de consulta y modificación de la Fecha de Caducidad
        public void setFechaCaducidad(Date d){
            fechaCaducidad = d;
        }
        
        public Date getFechaCaducidad(){
             return fechaCaducidad;
        }
        
        
        //Métodos de consulta y modificación de la Zona Tarifaria
        public void setZona(Zona z){
            zonaTarifaria = z;
        }
        public Zona getZona(){
            return zonaTarifaria;
        }
        
        
        //Imprimir Abono
        public void imprimeInformacion(){
            System.out.println("Nombre Abonado: "+nombreTitular);
            System.out.println("Apellidos Abonado: "+apellidosTitular);
            System.out.println("DNI Titular "+DNI);
            System.out.println("Año Nacimiento "+anioNacimiento);
            System.out.println("El Abono Caduca el "+fechaCaducidad);
            System.out.println("La Zona Tarifaria es "+zonaTarifaria);
            System.out.println("Familia Numerosa: "+familiaNum);
        }
        
        public double calcularImporte(double tarifaBase){
            double tarifa;
        //Calculo de la tarifa segun la zona, el valor se almacena en la variable tarifa
            if (this.getZona()==Zona.A1){
                 tarifa= tarifaBase;
            }else if (this.getZona()==Zona.A2){
                 tarifa = tarifaBase*1.25;
             }else if(this.getZona()==Zona.B1){
                 tarifa = tarifaBase*1.5;
             }else if(this.getZona()==Zona.B2){
                 tarifa = tarifaBase*2;
             }else{
                 tarifa = tarifaBase*3;
             }
             //Modifico la tarifa si el usuario es familia numerosa o menor de edad
            if(this.getFamiliaNumerosa()==true || Calendar.getInstance().YEAR -this.getAnioNacimiento()<=18){
                tarifa = tarifa*0.5;
            }
            
            return tarifa;
        
        }
        
        public void recarga(){
            Calendar d = Calendar.getInstance();//Fecha de hoy en variabe d
            d.add(Calendar.MONTH,1);//le sumo un mes a la fecha
            Date date=d.getTime();//transformo calendar en date
            this.setFechaCaducidad(date);//actualizo la fecha del abono
            System.out.println("El abono "+ getDNI() +"  ha sido recargado. \nLa nueva fecha de caducidad es: " + date);
        }
    
     
}

