import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class Lanzador{
    public static void main(String[] args){
        
        //ETAPA 1
        /*
        Abono a = new Abono();
        Zona z = Zona.A1;
        a.setZona(z);
        a.setNombre("Federico");
        a.setApellidos ("García Lopez");
        a.setDNI("43815923F");
        a.setFamiliaNumerosa (false);
        a.setAnioNacimiento (1974);
        a.imprimeInformacion();
        */
        
        //ETAPA 2.1
        /*
        Gestor g = new Gestor();
        g.actualizaTarifaBase(10);
        Abono a = new Abono();
        Zona z = Zona.A1;
        Date d = new Date(118, 5, 2, 0, 0, 0);
        a.setNombre("Federido");
        a.setApellidos("Garcia Lopez");
        a.setDNI("43815923F");
        a.setFamiliaNumerosa(true);
        a.setAnioNacimiento(1974);
        a.setFechaCaducidad(d);
        a.setZona(z);
        g.anyadirAbono(a);
        g.imprimeImporte("43815923F");
        g.imprimeImporte("54353543L");
        g.recargaAbono("43815923F");
        */
        
        //ETAPA 2.2
       /*
        Gestor g = new Gestor();
        g.actualizaTarifaBase(10);
        Abono a1 = new Abono();
        Zona z1 = Zona.A2;
        a1.setZona(z1);
        a1.setNombre("Luis");
        a1.setApellidos("López López");
        a1.setDNI("42523435A");
        a1.setFamiliaNumerosa(false);
        a1.setAnioNacimiento(2010);
        g.anyadirAbono(a1);
        g.imprimeImporte("42523435A");
        g.recargaAbono ("42523435A");
        Abono a2 = new AbonoAnual();
        Zona z2 = Zona.B2;
        a2.setZona(z2);
        a2.setNombre ("Pepe");
        a2.setApellidos ("García González");
        a2.setDNI ("5283843G");
        a2.setFamiliaNumerosa(false);
        a2.setAnioNacimiento(2004);
        g.anyadirAbono(a2);
        g.imprimeImporte("5283843G");
        g.recargaAbono("5283843G");
        Estacion e1=new Estacion();
        Estacion e2=new Estacion();
        e1.setLocalidad("Madrid-Atocha");
        e1.setZona(Zona.A1);
        e2.setLocalidad("San Rafael");
        e2.setZona(Zona.B2);
        g.anyadirEstacion(e1);
        g.anyadirEstacion(e2);
        e1.validarAbono(a1);
        e2.validarAbono(a1);
        e1.validarAbono(a2);
        e2.validarAbono(a2);
        */
        
        //ETAPA 3.2.
        
        Gestor g = new Gestor();
        Estacion e1 = new Estacion();
        Estacion e2 = new Estacion();
        e1.setLocalidad("Madrid-Atocha");
        e1.setZona (Zona.A1);
        e2.setLocalidad("San Rafael");
        e2.setZona (Zona.B2);
        g.anyadirEstacion (e1);
        g.anyadirEstacion (e2);
        g.zonaRequerida ("Madrid-Atocha","Valencia");
        g.zonaRequerida ("Madrid-Atocha","San Rafael");
        
        
       
       
    }
    
}
